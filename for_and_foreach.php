<html>
<head><title>FOR & FOREACH</title></head>
<body bgcolor="lavender">
	<center><h2>Menampilkan For dan Foreach</h2></center>
	<style>
		.div{
			border-top: 4px solid orange;
			border-bottom: 4px solid orange;
			border-left: 4px solid orange;
			border-right: 4px solid orange;
			background-color: salmon;
			height: 450px;
			width: 550px;
		}
	</style>
	<center><div class="div">
		<h2>Tampilan</h2>
<?php
// Menampilkan isi array dengan FOR dan FOREACH
$arrWarna = array("Red","Yellow","Green","Blue","Purple");

echo "Menampilkan isi array dengan FOR: <br>";
for($i = 0; $i< count($arrWarna); $i++){
	echo " Warna Pelangi <font color=$arrWarna[$i]>".$arrWarna[$i]."</font><br>";
}

echo "<br>Menampilkan isi array dengan FOREACH: <br>";
foreach($arrWarna as $Warna){
	echo "Warna Pelangi <font color=$Warna>".$Warna."</font><br>";
}
?></div></center>
</body>
</html>