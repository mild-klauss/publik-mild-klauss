<?php 
//fungsi dengan return value dan parameter 
function luas_lingkaran($jari){
	return 3.14*$jari*$jari;
}

//pemanggilan fungsi
$r=10;
echo "Luas lingkaran dengan jari-jari $r = ";
echo luas_lingkaran($r);

//tampilkan UDF dan fungsi yang didukung php versi saat ini 
function luas_lingkaran($jari){
	return 3.14*$jari*$jari;
}
 
 $arr=get_defined_functions();
 echo "<pre>";
 print_r($arr);
 echo "</pre>";


?>